const mongoose = require("mongoose");
const config = require("./config");
const {nanoid} = require("nanoid");
const User = require("./models/User");
const Establishment = require('./models/Establishment');
const Image = require('./models/Image');
const Comment = require('./models/Comment');

const run = async () => {
    await mongoose.connect(config.db.url, config.db.options);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for (const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    const [user, admin, john, lina] = await User.create(
        {
            email: "user@gmail.com",
            password: "123",
            token: nanoid(),
            role: "user",
            name: "user",
        },
        {
            email: "admin@gmail.com",
            password: "123",
            token: nanoid(),
            role: "admin",
            name: "Admin",
        },
        {
            email: "lina@gmail.com",
            password: "123",
            token: nanoid(),
            role: "user",
            name: "Lina",
        },
        {
            email: "john@gmail.com",
            password: "123",
            token: nanoid(),
            role: "user",
            name: "John",
        },
    );

    const [est1, est2, est3, est4] = await Establishment.create({
        title: "Villa Toscana",
        description: "Bubby’s opened on Thanksgiving Day 1990. Chef / Owner Ron Silver began baking pies and selling them to restaurants and his neighbors out of a small kitchen at the corner of Hudson and North Moore St. in Tribeca. Today, NYC’s beloved restaurant and pie shop celebrates 27 years of classic, made from scratch American cooking.",
        mainPic: "fixtures/1.jpeg",
        user: admin
    },{
        title: "Rattatuy",
        description: "Bubby’s opened on Thanksgiving Day 1990. Chef / Owner Ron Silver began baking pies and selling them to restaurants and his neighbors out of a small kitchen at the corner of Hudson and North Moore St. in Tribeca. Today, NYC’s beloved restaurant and pie shop celebrates 27 years of classic, made from scratch American cooking.",
        mainPic: "fixtures/1.jpeg",
        user: user

    },{
        title: "Nusr-Et",
        description: "Bubby’s opened on Thanksgiving Day 1990. Chef / Owner Ron Silver began baking pies and selling them to restaurants and his neighbors out of a small kitchen at the corner of Hudson and North Moore St. in Tribeca. Today, NYC’s beloved restaurant and pie shop celebrates 27 years of classic, made from scratch American cooking.",
        mainPic: "fixtures/1.jpeg",
        user: john

    },{
        title: "Frunze",
        description: "Bubby’s opened on Thanksgiving Day 1990. Chef / Owner Ron Silver began baking pies and selling them to restaurants and his neighbors out of a small kitchen at the corner of Hudson and North Moore St. in Tribeca. Today, NYC’s beloved restaurant and pie shop celebrates 27 years of classic, made from scratch American cooking.",
        mainPic: "fixtures/1.jpeg",
        user: lina
    });

    const [img1, img2, img3, img4] = await Image.create({
        establishment: est1,
        user: user,
        image: "fixtures/night.jpeg"
    },{
        establishment: est2,
        user: admin,
        image: "fixtures/lunch.jpeg"
    },{
        establishment: est3,
        user: user,
        image: "fixtures/breakfast.jpeg"
    },{
        establishment: est4,
        user: admin,
        image: "fixtures/dinner.jpeg"
    });

    const [com1, com2, com3, com4] = await Comment.create({
        user: admin,
        establishment: est1,
        description: "Tasty food",
        date: Date.now(),
        foodComment: 5,
        serviceComment: 4,
        interiorComment: 3
    },{
        user: user,
        establishment: est2,
        description: "Expensive",
        date: Date.now(),
        foodComment: 1,
        serviceComment: 2,
        interiorComment: 3
    },{
        user: john,
        establishment: est3,
        description: "Expensive",
        date: Date.now(),
        foodComment: 1,
        serviceComment: 2,
        interiorComment: 3
    },{
        user: lina,
        establishment: est4,
        description: "Expensive",
        date: Date.now(),
        foodComment: 1,
        serviceComment: 2,
        interiorComment: 3
    });

    await Establishment.findByIdAndUpdate(est1, {$push: {images: img1, comments: com1}});
    await Establishment.findByIdAndUpdate(est2, {$push: {images: img2, comments: com2}});
    await Establishment.findByIdAndUpdate(est3, {$push: {images: img3, comments: com3}});
    await Establishment.findByIdAndUpdate(est4, {$push: {images: img4, comments: com4}});

    await User.findByIdAndUpdate(admin, {$push: {images: img1, comments: com1, establishments: est1}});
    await User.findByIdAndUpdate(user, {$push: {images: img2, comments: com2, establishments: est2}});
    await User.findByIdAndUpdate(john, {$push: {images: img3, comments: com3, establishments: est3}});
    await User.findByIdAndUpdate(lina, {$push: {images: img4, comments: com4, establishments: est4}});

    await mongoose.connection.close();
};

run().catch(console.error);

