const express = require('express');
const multer = require("multer");
const config = require("../config");
const {nanoid} = require("nanoid");
const path = require("path");
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const Establishment = require('../models/Establishment');
const Image = require('../models/Image');
const Comment = require('../models/Comment');
const User = require('../models/User');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const establishments = await Establishment
            .find()
            .populate({
                path: 'user images comments',
                select: 'name image establishment date description foodComment serviceComment interiorComment'
            });

        res.send(establishments);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.get('/:id', async (req, res) => {
    try {
        const establishment = await Establishment
            .findById(req.params.id).populate({
                path: 'comments',
                populate: {path: 'user', select: 'name'}
            }).populate({
                path: 'user',
                select: 'name image establishment'
            }).populate({
                path: 'images',
                populate: {path: 'user', select: 'name'}
            });

        if (!establishment) return res.status(404).send({message: "Заведение не найдено"});

        res.send(establishment);
    } catch (e) {
        res.status(500).send(e);
    }
})

router.post('/', auth, permit('user', 'admin'), upload.single('mainPic'), async (req, res) => {
    try {
        const establishmentData = {
            user: req.user._id,
            title: req.body.title,
            description: req.body.description,
            mainPic: 'uploads/' +  req.file.filename,
        };

        const establishment = new Establishment(establishmentData);

        await establishment.save();

        await User.findByIdAndUpdate(req.user._id.toString(), {$push: {establishments: establishment._id}});

        res.send({message: "Заведение добавлено!"});
    } catch (e) {
        console.log(e)
        res.status(500).send(e);
    }
});

router.put('/:id', auth, permit('user', 'admin'), upload.single('image'), async (req, res) => {
    const imageQuery = req.query.image;
    const commentQuery = req.query.comment;
    const foodComment = req.body.foodComment;
    const serviceComment = req.body.serviceComment;
    const interiorComment = req.body.interiorComment;

    try {
        if (imageQuery) {
            const imageData = {
                establishment: req.params.id,
                user: req.user._id,
                image: 'uploads/' + req.file.filename,
            };

            const image = new Image(imageData);
            await image.save();

            await Establishment.findByIdAndUpdate(req.params.id, {$push: {images: image._id}});

            res.send({message: "Изображение добавлено!"});
        }

        if (commentQuery) {
            const establishment = await Establishment.findById(req.params.id).populate({path: 'comments', select: 'user'});

            const findComment = establishment.comments.filter(item => item.user.toString() === req.user._id.toString());

            if (findComment.length !== 0) return res.send({warning: "Вы не можете оставить отзыв, удалите предыдущий"});

            const commentData = {
                establishment: req.params.id,
                user: req.user._id,
                description: req.body.description,
            }

            if (foodComment) commentData.foodComment = foodComment;
            if (serviceComment) commentData.serviceComment = serviceComment;
            if (interiorComment) commentData.interiorComment = interiorComment;

            const comment = new Comment(commentData);
            await comment.save();

            await Establishment.findByIdAndUpdate(req.params.id, {$push: {comments: comment._id}});

            res.send({message: "Отзыв добавлен!"});
        }
    } catch (e) {
        res.status(500).send(e);
    }
});

router.delete('/:id', auth, permit('admin'), async (req, res) => {
    try {
        const establishment = await Establishment.findById(req.params.id);

        if (!establishment) return res.status(404).send({message: "Заведение не найдено!"});

        await User.findByIdAndUpdate(establishment.user.toString(), {$pull: {establishments: req.params.id}});

        await Establishment.findByIdAndDelete(req.params.id);

        res.send({message: "Удалено успешно!"});
    } catch (e) {
        res.status(500).send(e);
    }
});

module.exports = router;