const express = require('express');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const Comment = require('../models/Comment');
const Establishment = require('../models/Establishment');
const User = require('../models/User');

const router = express.Router();

router.delete('/:id', auth, permit('user', 'admin'), async (req, res) => {
    try {
        const comment = await Comment.findById(req.params.id);

        if (!comment) return res.status(404).send({message: "Отзыв не найден!"});

        if (req.user._id.toString() === comment.user.toString()) {
            await Establishment.findByIdAndUpdate(comment.establishment.toString(), {$pull: {comments: req.params.id}});

            await User.findByIdAndUpdate(comment.user.toString(), {$pull: {comments: req.params.id}});

            await Comment.findByIdAndDelete(req.params.id);

            return res.send({message: "Отзыв удален!"});
        }

        if (req.user.role === 'admin') {
            await Establishment.findByIdAndUpdate(comment.establishment.toString(), {$pull: {_id: req.params.id}});

            await User.findByIdAndUpdate(comment.user.toString(), {$pull: {comments: req.params.id}});

            await Comment.findByIdAndDelete(req.params.id);

            return res.send({message: "Отзыв удален!"});
        }
    } catch (e) {
        res.status(500).send(e);
    }
});

module.exports = router;