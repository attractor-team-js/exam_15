const express = require('express');
const Image = require('../models/Image');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const Establishment = require('../models/Establishment');
const User = require('../models/User');

const router = express.Router();

router.delete('/:id', auth, permit('admin'), async (req, res) => {
    try {
        const image = await Image.findById(req.params.id);

        if (!image) return res.status(404).send({message: "Изображение не найдено"});

        await Establishment.findByIdAndUpdate(image.establishment, {$pull: {images: req.params.id}});

        await User.findByIdAndUpdate(image.user, {$pull: {images: req.params.id}});

        await Image.findByIdAndDelete(req.params.id);

        res.send({message: "Изображение удалено"});
    } catch (e) {
        res.status(500).send(e);
    }
});

module.exports = router;