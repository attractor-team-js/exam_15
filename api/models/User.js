const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const {nanoid} = require("nanoid");

const SALT_WORK_FACTOR = 10;

const validateUnique = async value => {
    const user = await User.findOne({email: value});
    if (user) return false;
};

const validateEmail = value => {
    const re = /^(\w+[-.]?\w+)@(\w+)([.-]?\w+)?(\.[a-zA-Z]{2,})$/;
    if (!re.test(value)) return false;
};

const UserSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        unique: true,
        trim: true,
        lowercase: true,
        validate: [
            {validator: validateEmail, message: 'Неподходящий email'},
            {validator: validateUnique, message: 'Этот пользователь уже зарегистрирован'}
        ],
    },
    password: {
        type: String,
        required: true,
        trim: true,
    },
    token: {
        type: String,
        trim: true,
        required: true,
    },
    role: {
        type: String,
        required: true,
        trim: true,
        default: 'user',
        enum: ['admin', 'user'],
    },
    name: {
        type: String,
        trim: true,
        required: 'Это поле является обязательным',
    },
    establishments: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Establishment",
    }],
    images: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Image",
    }],
    comments: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Comment"
    }],
});

UserSchema.pre('save', async function (next) {
    if (!this.isModified('password')) return next();

    const salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
    this.password = await bcrypt.hash(this.password, salt);

    next();
});

UserSchema.set('toJSON', {
    transform: (doc, ret,) => {
        delete ret.password;
        return ret;
    },
});

UserSchema.methods.checkPassword = function (password) {
    return bcrypt.compare(password, this.password);
};

UserSchema.methods.generateToken = function () {
    this.token = nanoid();
};

const User = mongoose.model('User', UserSchema);

module.exports = User;