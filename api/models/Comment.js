const mongoose = require('mongoose');

const CommentSchema = new mongoose.Schema({
    establishment: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Establishment",
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
    },
    description: {
        type: String,
        required: true,
    },
    date: {
        type: Date,
        default: Date.now(),
    },
    foodComment: {
        type: Number,
        default: 0
    },
    serviceComment: {
        type: Number,
        default: 0
    },
    interiorComment: {
        type: Number,
        default: 0
    },
});

const Comment = mongoose.model('Comment', CommentSchema);

module.exports = Comment;