const mongoose = require('mongoose');

const EstablishmentSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    },
    mainPic: {
        type: String,
        required: true,
    },
    images: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Image",
    }],
    comments: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Comment"
    }],
    title: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true
    }
});

const Establishment = mongoose.model('Establishment', EstablishmentSchema);

module.exports = Establishment;