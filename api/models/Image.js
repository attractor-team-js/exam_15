const mongoose = require('mongoose');

const ImageSchema = new mongoose.Schema({
    establishment: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Establishment",
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
    },
    image: {
        type: String,
        required: true,
    },
});

const Image = mongoose.model('Image', ImageSchema);

module.exports = Image;