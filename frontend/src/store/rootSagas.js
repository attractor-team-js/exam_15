import {all} from 'redux-saga/effects';
import usersSagas from "./sagas/usersSagas";
import establishmentsSagas from "./sagas/establishmentsSagas";
import imagesSagas from "./sagas/imagesSagas";
import commentsSagas from "./sagas/commentsSagas";

export function* rootSagas() {
    yield all([
        ...usersSagas,
        ...establishmentsSagas,
        ...imagesSagas,
        ...commentsSagas,
    ]);
}