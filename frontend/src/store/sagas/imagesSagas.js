import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import {put, takeEvery} from "redux-saga/effects";
import {deleteImage, deleteImageFailure, deleteImageSuccess} from "../actions/imagesActions";
import {fetchEstablishment} from "../actions/establishmentsActions";

export function* deleteImageSagas({payload}) {
    try {
        const {data} = yield axiosApi.delete(`/images/${payload.image}`);
        toast.success(data.message);
        yield put(deleteImageSuccess());
        yield put(fetchEstablishment(payload.id));
    } catch (e) {
        yield put(deleteImageFailure(e));
    }
}

const imagesSagas = [
    takeEvery(deleteImage, deleteImageSagas)
];

export default imagesSagas;