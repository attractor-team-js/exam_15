import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import {put, takeEvery} from "redux-saga/effects";
import {deleteComment, deleteCommentFailure, deleteCommentSuccess} from "../actions/commentsActions";
import {fetchEstablishment} from "../actions/establishmentsActions";

export function* deleteCommentSagas({payload}) {
    try {
        const {data} = yield axiosApi.delete(`/comments/${payload.comment}`);
        toast.success(data.message);
        yield put(deleteCommentSuccess());
        yield put(fetchEstablishment(payload.id));
    } catch (e) {
        toast.error(e.response.data.errors.description.message);
        yield put(deleteCommentFailure(e.response.data));
    }
}

const commentsSagas = [
    takeEvery(deleteComment, deleteCommentSagas)
];

export default commentsSagas;