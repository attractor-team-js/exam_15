import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import History from '../../History';
import {put, takeEvery} from "redux-saga/effects";
import {
    createEstablishments,
    createEstablishmentsFailure,
    createEstablishmentsSuccess,
    deleteEstablishments,
    deleteEstablishmentsFailure,
    deleteEstablishmentsSuccess, fetchEstablishment, fetchEstablishmentFailure,
    fetchEstablishments,
    fetchEstablishmentsFailure,
    fetchEstablishmentsSuccess, fetchEstablishmentSuccess,
    putEstablishments,
    putEstablishmentsFailure,
    putEstablishmentsSuccess
} from "../actions/establishmentsActions";

export function* fetchEstablishmentsSaga() {
    try {
        const {data} = yield axiosApi.get('/establishments');
        yield put(fetchEstablishmentsSuccess(data));
    } catch (e) {
        toast.error(e.response.data.errors.description.message);
        yield put(fetchEstablishmentsFailure(e.response.data));
    }
}

export function* fetchEstablishmentSaga({payload}) {
    try {
        const {data} = yield axiosApi.get(`/establishments/${payload}`);
        yield put(fetchEstablishmentSuccess(data));
    } catch (e) {
        toast.error(e.response.data.errors.description.message);
        yield put(fetchEstablishmentFailure(e.response.data));
    }
}

export function* createEstablishmentsSaga({payload}) {
    try {
        const {data} = yield axiosApi.post(`/establishments`, payload);
        yield put(fetchEstablishments());
        toast.success(data.message);
        yield put(createEstablishmentsSuccess());
        History.push('/');
    } catch (e) {
        toast.error(e.response.data.errors.description.message);
        yield put(createEstablishmentsFailure(e.response.data));
    }
}

export function* putEstablishmentsSaga({payload}) {
    const image = payload.image;
    const comment = payload.comment;

    let url = `/establishments/${payload.id}`

    if (image) url = url.concat(`?image=${image}`);
    if (comment) url = url.concat(`?comment=${comment}`);
    try {
        const {data} = yield axiosApi.put(url, payload.data);
        yield put(fetchEstablishment(payload.id));
        if (data.warning) {
            toast.warning(data.warning);
        } else {
            toast.success(data.message);
        }
        yield put(putEstablishmentsSuccess());
    } catch (e) {
        toast.error(e.response.data.errors.description.message);
        yield put(putEstablishmentsFailure(e.response.data));
    }
}

export function* deleteEstablishmentsSaga({payload}) {
    try {
        const {data} = yield axiosApi.delete(`/establishments/${payload}`);
        yield put(fetchEstablishments());
        toast.success(data.message);
        yield put(deleteEstablishmentsSuccess());
    } catch (e) {
        toast.error(e.response.data.errors.description.message);
        yield put(deleteEstablishmentsFailure(e.response.data));
    }
}

const establishmentsSagas = [
    takeEvery(fetchEstablishments, fetchEstablishmentsSaga),
    takeEvery(fetchEstablishment, fetchEstablishmentSaga),
    takeEvery(createEstablishments, createEstablishmentsSaga),
    takeEvery(putEstablishments, putEstablishmentsSaga),
    takeEvery(deleteEstablishments, deleteEstablishmentsSaga),
];

export default establishmentsSagas;