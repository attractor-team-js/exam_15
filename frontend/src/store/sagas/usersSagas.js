import {
    loginUser,
    loginUserFailure,
    loginUserSuccess,
    logout,
    registerUser,
    registerUserFailure,
    registerUserSuccess,
} from "../actions/usersActions";
import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import {put, takeEvery} from "redux-saga/effects";

export function* registerUserSaga({payload: user}) {
    try {
        const userData = {
            email: user.email,
            name: user.name,
            password: user.password,
        };

        const response = yield axiosApi.post('/users', userData);
        yield put(registerUserSuccess(response.data));
        user.navigate('/', true);
        toast.success('Вы усрешно зарегестрировались!')
    } catch (e) {
        toast.error(e.response.data.global);
        yield put(registerUserFailure(e.response.data));
    }
}

export function* loginUserSaga({payload: user}) {
    try {
        const {data} = yield axiosApi.post('/users/sessions', user);
        yield put(loginUserSuccess(data));
        user.navigate('/', true);
        toast.success('Вы авторизированы!');
    } catch (e) {
        toast.error(e.response);
        yield put(loginUserFailure(e.response.data));
    }
}

export function* logoutUserSaga() {
    try {
        yield axiosApi.delete('/users/sessions');
    } catch (e) {
        if (e.response && e.response.data) {
            yield  put(loginUserFailure(e.response.data));
        } else {
            yield put(loginUserFailure({message: "No internet connexion"}));
        }
    }
}

const usersSagas = [
    takeEvery(registerUser, registerUserSaga),
    takeEvery(loginUser, loginUserSaga),
    takeEvery(logout, logoutUserSaga),
];

export default usersSagas;