import {createSlice} from "@reduxjs/toolkit";

export const initialState = {
    user: null,
    users: [],
    registerLoading: false,
    registerError: null,
    loginError: null,
    loginLoading: false,
    loadUserDate: false,
    userDate: null,
    userError: null,
    payment: null,
    total: 0,
    resetError:null,
    resetLoading: false,
    forgotError:null,
    forgotLoading: false,
    notification: false,
    editLoading: false,
    editError: null,
};

const name = 'users';

const usersSlice = createSlice({
    name,
    initialState,
    reducers: {
        registerUser(state) {
            state.registerLoading = true;
        },
        registerUserSuccess(state, {payload: userData}) {
            state.user = userData === undefined ? state.user : userData;
            state.registerLoading = false;
            state.registerError = null;
        },
        registerUserFailure(state, action) {
            state.registerLoading = false;
            state.registerError = action.payload;
        },
        loginUser(state) {
            state.loginLoading = true;
        },
        loginUserSuccess(state, action) {
            state.loginLoading = false;
            state.user = action.payload;
            state.loginError = null;
        },
        loginUserFailure(state, action) {
            state.loginError = action.payload;
            state.loginLoading = false;
        },
        clearError(state) {
            state.loginError = null;
            state.registerError = null;
            state.error = null;
            state.userError = null;
        },
        logout(state) {
            state.user = null;
        },
    },
});

export default usersSlice;