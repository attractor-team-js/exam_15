import {createSlice} from "@reduxjs/toolkit";

export const initialState = {
    loading: false,
    error: null
};

const name = 'images';

const imagesSlice = createSlice({
    name,
    initialState,
    reducers: {
        deleteImage(state) {
            state.loading = true;
        },
        deleteImageSuccess(state) {
            state.loading = false;
        },
        deleteImageFailure(state, action) {
            state.loading = false;
            state.error = action.payload;
        },
    },
});

export default imagesSlice;