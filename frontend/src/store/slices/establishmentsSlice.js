import {createSlice} from "@reduxjs/toolkit";

export const initialState = {
    establishment: null,
    establishments: [],
    loading: false,
    error: null
};

const name = 'establishments';

const establishmentsSlice = createSlice({
    name,
    initialState,
    reducers: {
        fetchEstablishments(state) {
            state.loading = true;
        },
        fetchEstablishmentsSuccess(state, action) {
            state.loading = false;
            state.establishments = action.payload;
        },
        fetchEstablishmentsFailure(state, action) {
            state.loading = false;
            state.error = action.payload;
        },
        createEstablishments(state) {
            state.loading = true;
        },
        createEstablishmentsSuccess(state) {
            state.loading = false;
        },
        createEstablishmentsFailure(state, action) {
            state.loading = false;
            state.error = action.payload;
        },
        putEstablishments(state) {
            state.loading = true;
        },
        putEstablishmentsSuccess(state) {
            state.loading = false;
        },
        putEstablishmentsFailure(state, action) {
            state.loading = false;
            state.error = action.payload;
        },
        deleteEstablishments(state) {
            state.loading = true;
        },
        deleteEstablishmentsSuccess(state) {
            state.loading = false;
        },
        deleteEstablishmentsFailure(state, action) {
            state.loading = false;
            state.error = action.payload;
        },
        fetchEstablishment(state) {
            state.loading = true;
        },
        fetchEstablishmentSuccess(state, action) {
            state.loading = false;
            state.establishment = action.payload;
        },
        fetchEstablishmentFailure(state, action) {
            state.loading = false;
            state.error = action.payload;
        },
    },
});

export default establishmentsSlice;