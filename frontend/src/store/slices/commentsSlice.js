import {createSlice} from "@reduxjs/toolkit";

export const initialState = {
    loading: false,
    error: null
};

const name = 'comments';

const commentsSlice = createSlice({
    name,
    initialState,
    reducers: {
        deleteComment(state) {
            state.loading = true;
        },
        deleteCommentSuccess(state) {
            state.loading = false;
        },
        deleteCommentFailure(state, action) {
            state.loading = false;
            state.error = action.payload;
        },
    },
});

export default commentsSlice;