import establishmentsSlice from "../slices/establishmentsSlice";

export const {
    fetchEstablishments,
    fetchEstablishmentsSuccess,
    fetchEstablishmentsFailure,
    deleteEstablishments,
    deleteEstablishmentsSuccess,
    deleteEstablishmentsFailure,
    putEstablishments,
    putEstablishmentsSuccess,
    putEstablishmentsFailure,
    createEstablishments,
    createEstablishmentsSuccess,
    createEstablishmentsFailure,
    fetchEstablishment,
    fetchEstablishmentSuccess,
    fetchEstablishmentFailure
} = establishmentsSlice.actions;