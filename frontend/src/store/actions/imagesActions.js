import imagesSlice from "../slices/imagesSlice";

export const {
    deleteImage,
    deleteImageSuccess,
    deleteImageFailure
} = imagesSlice.actions;