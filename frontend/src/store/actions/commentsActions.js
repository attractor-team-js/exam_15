import commentsSlice from "../slices/commentsSlice";

export const {
    deleteComment,
    deleteCommentSuccess,
    deleteCommentFailure
} = commentsSlice.actions;