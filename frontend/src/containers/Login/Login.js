import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import FormElement from "../../components/UI/Form/FormElement";
import {clearError, loginUser} from "../../store/actions/usersActions";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import Alert from '@mui/material/Alert';
import {AlertTitle, Box} from "@mui/material";
import Button from "@mui/material/Button";
import {Link, useNavigate} from "react-router-dom";

const Login = () => {
    const dispatch = useDispatch();
    const loading = useSelector(state => state.users.loginLoading);
    const error = useSelector(state => state.users.loginError);
    const navigate = useNavigate();

    const [user, setUser] = useState({
        email: '',
        password: ''
    });

    useEffect(() => {
        return () => {
            dispatch(clearError());
        };
    }, [dispatch]);

    const handleChange = e => {
        const {name, value} = e.target;

        setUser(prevState => ({...prevState, [name]: value}));
    };

    const handleSubmit = e => {
        e.preventDefault();
        dispatch(loginUser({...user, navigate}));
    };

    return (
        <Container component="section" maxWidth="xs">
            <Box sx={{paddingTop: "50px"}}>
                {
                    error &&
                    <Alert align="center" severity="error">
                        <AlertTitle>{error.message || error.global}</AlertTitle>
                    </Alert>
                }

                <Grid
                    component="form"
                    container
                    direction="column"
                    onSubmit={handleSubmit}
                    spacing={2}
                >
                    <FormElement
                        fullWidth
                        type="email"
                        autoComplete="current-email"
                        label="Эл.почта"
                        name="email"
                        value={user.email}
                        required={true}
                        onChange={handleChange}
                    />

                    <FormElement
                        fullWidth
                        autoComplete="current-password"
                        label="Пароль"
                        name="password"
                        value={user.password}
                        required={true}
                        onChange={handleChange}
                    />

                    <Grid item xs={12}>
                        <ButtonWithProgress
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            loading={loading}
                        >
                            войти
                        </ButtonWithProgress>
                    </Grid>

                    <Grid item xs={12}>
                        <Button
                            component={Link}
                            variant="body2"
                            to='/register'
                            fullWidth
                        >
                            Нет аккаунта? Зарегистрироваться
                        </Button>
                    </Grid>
                </Grid>
            </Box>
        </Container>
    );
};

export default Login;