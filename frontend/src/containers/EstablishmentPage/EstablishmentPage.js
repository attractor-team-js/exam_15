import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import {useDispatch, useSelector} from "react-redux";
import React, {useEffect} from "react";
import {fetchEstablishment} from "../../store/actions/establishmentsActions";
import {useParams} from "react-router-dom";
import {Box, Divider, Rating, Typography} from "@mui/material";
import {apiURL} from "../../config";
import ReviewsList from "../../components/Lists/ReviewsList/ReviewsList";
import RatingForm from "../../components/RatingForm/RatingForm";
import ImageUploadForm from "../../components/ImageUploadForm/ImageUploadForm";
import MasonryElem from "../../components/MasonryElem/MasonryElem";

const EstablishmentPage = () => {
    const dispatch = useDispatch();
    const params = useParams();
    const user = useSelector(state => state.users.user);
    const establishment = useSelector(state => state.establishments.establishment);

    const ratings = establishment &&  {
        overall: establishment.comments.reduce((acc, cur) => {
            return acc + (cur.foodComment + cur.interiorComment + cur.serviceComment)/3;
        }, 0),
        food: establishment.comments.reduce((acc, cur) => {
            return acc + (cur.foodComment)/ establishment.comments.length;
        }, 0),
        service: establishment.comments.reduce((acc, cur) => {
            return acc + (cur.serviceComment)/ establishment.comments.length;
        }, 0),
        interior: establishment.comments.reduce((acc, cur) => {
            return acc + (cur.interiorComment)/ establishment.comments.length;
        }, 0),
    }

    useEffect(() => {
        dispatch(fetchEstablishment(params.id));
    }, [dispatch])

    return (
        establishment &&
        <Container>
            <Grid container sx={{paddingY: '30px'}}>
                <Grid item xs={12} md={5} lg={5} alignItems="center">
                    <Typography variant="h4">
                        {establishment.title}
                    </Typography>

                    <Grid item xs={12} sm={5} lg={5}>
                        <Box sx={{width: "300px"}}>
                            <img src={apiURL + '/' + establishment.mainPic} alt="Kitchen" width="100%" height="auto"/>
                        </Box>
                    </Grid>

                    <Typography variant="body1">
                        {establishment.description}
                    </Typography>
                </Grid>

                <Grid item xs={12} md={6} lg={6}>
                    <MasonryElem images={establishment.images} id={establishment._id}/>
                </Grid>

                <Grid item xs={12} md={12} lg={12}>
                    <Typography variant="h6">
                        Ratings
                    </Typography>

                    <Grid container flexDirection="column">
                        <Grid item xs={12} sm={6} md={4} display="flex" alignItems="center">
                            <Typography variant="body2" component="div" sx={{flexGrow: 1}}>
                                Overall:
                            </Typography>

                            <Rating value={ratings.overall} readOnly/>
                            <strong>({ratings.overall})</strong>
                        </Grid>

                        <Grid item xs={12} sm={6} md={4} display="flex" alignItems="center">
                            <Typography variant="body2" component="div" sx={{flexGrow: 1}}>
                                Quality of food:
                            </Typography>

                            <Rating value={ratings.food} readOnly/>
                            <strong>({ratings.food})</strong>
                        </Grid>

                        <Grid item xs={12} sm={6} md={4} display="flex" alignItems="center">
                            <Typography variant="body2" component="div" sx={{flexGrow: 1}}>
                                Service quality:
                            </Typography>

                            <Rating value={ratings.service} readOnly/>
                            <strong>({ratings.service})</strong>
                        </Grid>

                        <Grid item xs={12} sm={6} md={4} display="flex" alignItems="center">
                            <Typography variant="body2" component="div" sx={{flexGrow: 1}}>
                                Interior:
                            </Typography>

                            <Rating value={ratings.interior} readOnly/>
                            <strong>({ratings.interior})</strong>
                        </Grid>
                    </Grid>
                </Grid>

                <Grid item xs={12} md={12} lg={12}>
                    <Divider/>

                    <Typography variant="h6">
                        Reviews
                    </Typography>

                    <ReviewsList reviews={establishment.comments}/>
                </Grid>

                {user &&
                    <>
                        <Grid item xs={12} md={12} lg={12}>
                            <RatingForm id={establishment._id}/>
                        </Grid>

                        <Grid item xs={12} md={12} lg={12}>
                            <ImageUploadForm id={establishment._id}/>
                        </Grid>
                    </>}
            </Grid>
        </Container>

    );
};

export default EstablishmentPage;