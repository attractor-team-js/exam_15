import React, {useState} from 'react';
import Grid from "@mui/material/Grid";
import {Checkbox, FormControlLabel, FormGroup, TextField, Typography} from "@mui/material";
import FileInput from "../../components/UI/FileInput/FileInput";
import Container from "@mui/material/Container";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";
import {useDispatch, useSelector} from "react-redux";
import {createEstablishments} from "../../store/actions/establishmentsActions";

const AddEstablishmentPage = () => {
    const dispatch = useDispatch();
    const loading = useSelector(state => state.establishments.loading);
    const error = useSelector(state => state.establishments.error);
    const [checked, setChecked] = useState(false);
    const [value, setValue] = useState({
        title: "",
        description: "",
        mainPic: null
    });

    const handleChangeCheckBox = (e) => setChecked(e.target.checked);

    const handleChange = (e) => {
        const {name, value} = e.target;
        setValue(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setValue(prevState => {
            return {...prevState, [name]: file};
        });
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        const formData = new FormData();

        Object.keys(value).forEach(key => {
            formData.append(key, value[key]);
        });
        dispatch(createEstablishments(formData));
    }

    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    return (
        <Container maxWidth='xs'
        >
            <Grid
                container
                flexDirection="column"
                spacing={2}
                sx={{paddingTop: "30px"}}
                component="form"
            >
                <Grid item xs={12}>
                    <Typography variant="h5">
                        Add new place
                    </Typography>
                </Grid>

                <Grid item xs={12}>
                    <TextField
                        onChange={handleChange}
                        name="title"
                        label="Title"
                        fullWidth
                        error={getFieldError('title')}
                    />
                </Grid>

                <Grid item xs={12}>
                    <TextField
                        onChange={handleChange}
                        name="description"
                        label="Description"
                        fullWidth
                        multiline
                        rows={4}
                        error={getFieldError('description')}
                    />
                </Grid>

                <Grid item xs={12}>
                    <FileInput
                        fullWidth
                        label="Main photo"
                        name="mainPic"
                        onChange={fileChangeHandler}
                    />
                </Grid>

                <Grid item xs={12}>
                    <Typography>
                        By submitting this form, you confirm that you have read and agree to the official rules
                        and terms.
                    </Typography>
                    <FormGroup>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    checked={checked}
                                    onChange={handleChangeCheckBox}
                                />}
                            label="I understand"
                        />
                    </FormGroup>
                </Grid>

                <Grid item xs={12}>
                    <ButtonWithProgress
                        disabled={!checked}
                        loading={loading}
                        fullWidth
                        onClick={handleSubmit}
                        variant="contained"
                        type="submit"
                    >
                        Create
                    </ButtonWithProgress>
                </Grid>
            </Grid>
        </Container>
    );
};

export default AddEstablishmentPage;