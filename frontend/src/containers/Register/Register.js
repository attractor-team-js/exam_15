import React, {useEffect, useState} from 'react';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Container from "@mui/material/Container";
import Avatar from "@mui/material/Avatar";
import Grid from "@mui/material/Grid";
import {useDispatch, useSelector} from "react-redux";
import {clearError, registerUser} from "../../store/actions/usersActions";
import FormElement from "../../components/UI/Form/FormElement";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";
import {Box} from "@mui/material";
import Button from "@mui/material/Button";
import {Link, useNavigate} from "react-router-dom";

const Register = () => {
    const dispatch = useDispatch();
    const error = useSelector(state => state.users.registerError);
    const loading = useSelector(state => state.users.registerLoading);
    const navigate = useNavigate();

    const [user, setUser] = useState({
        email: '',
        password: '',
        name: '',
    });

    useEffect(() => {
        return () => {
            dispatch(clearError());
        };
    }, [dispatch]);

    const handleChange = e => {
        const {name, value} = e.target;

        setUser(prevState => ({...prevState, [name]: value}));
    };

    const handleSubmit = e => {
        e.preventDefault();
        dispatch(registerUser({...user, navigate}));
    };

    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    return (
        <Container component="section" maxWidth="xs">
            <Box sx={{paddingTop: "50px"}}>
                <Grid container justifyContent="center" sx={{marginBottom: "30px"}}>
                    <Avatar>
                        <LockOutlinedIcon/>
                    </Avatar>
                </Grid>

                <Grid
                    component="form"
                    container
                    onSubmit={handleSubmit}
                    justifyContent="center"
                    spacing={2}
                    noValidate
                >

                    <FormElement
                        fullWidth
                        xs={12}
                        required
                        variant="outlined"
                        type="text"
                        label="ФИО"
                        name="name"
                        value={user.name}
                        onChange={handleChange}
                        error={getFieldError('name')}
                    />

                    <FormElement
                        fullWidth
                        xs={12}
                        required
                        type="email"
                        autoComplete="new-email"
                        label="Эл.почта"
                        name="email"
                        value={user.email}
                        onChange={handleChange}
                        error={getFieldError('email')}
                    />

                    <FormElement
                        fullWidth
                        xs={12}
                        required
                        autoComplete="new-password"
                        label="Пароль"
                        name="password"
                        value={user.password}
                        onChange={handleChange}
                        error={getFieldError('password')}
                    />

                    <Grid item xs={12}>
                        <ButtonWithProgress
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            loading={loading}
                        >
                            зарегистрироваться
                        </ButtonWithProgress>
                    </Grid>

                    <Grid item xs={12}>
                        <Button
                            component={Link}
                            to='/login'
                            variant="body2"
                            fullWidth
                        >
                            Уже есть аккаунт? Войти
                        </Button>
                    </Grid>
                </Grid>
            </Box>
        </Container>
    );
};

export default Register;