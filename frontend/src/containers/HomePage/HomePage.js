import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchEstablishments} from "../../store/actions/establishmentsActions";
import Container from "@mui/material/Container";
import EstablishmentsList from "../../components/Lists/EstablishmentsList/EstablishmentsList";
import Grid from "@mui/material/Grid";

const HomePage = () => {
    const dispatch = useDispatch();
    const establishments = useSelector(state => state.establishments.establishments);

    useEffect(() => {
        dispatch(fetchEstablishments());
    }, [dispatch]);

    return (
        <Container>
            <Grid container spacing={2} sx={{paddingTop: "30px"}}>
                <EstablishmentsList items={establishments}/>
            </Grid>
        </Container>
    );
};

export default HomePage;