import React from 'react';
import {CssBaseline} from "@mui/material";
import AppToolbar from "../AppToolbar/AppToolbar";

const Layout = ({children}) => {
    return (
        <>
            <CssBaseline/>
            <AppToolbar/>
            <main>
                {children}
            </main>
        </>
    );
};

export default Layout;