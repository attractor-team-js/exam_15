import React from 'react';
import {AppBar, Toolbar} from "@mui/material";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {logout} from "../../../store/actions/usersActions";

const AppToolbar = () => {
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);

    const handleClick = () => {
        dispatch(logout());
    }

    return (
        <AppBar position="static">
            <Toolbar>
                <Grid container justifyContent="space-between">
                    <Grid item>
                        <Button component={Link} to="/" color="inherit">
                            Cafe critic
                        </Button>
                    </Grid>
                    <Grid item>
                        {user ?
                            <Grid container spacing={2}>
                                <Grid item>
                                    <Button
                                        variant="contained"
                                        color="success"
                                        component={Link}
                                        to="/addEstablishment"
                                    >
                                        Add place
                                    </Button>
                                </Grid>

                                <Grid item>
                                    <Button
                                        color="inherit"
                                        onClick={handleClick}
                                    >
                                        Log out
                                    </Button>
                                </Grid>
                            </Grid>
                            :
                            <Button
                                color="inherit"
                                component={Link}
                                to="/login"
                            >
                                Login
                            </Button>
                        }
                    </Grid>
                </Grid>
            </Toolbar>
        </AppBar>
    );
};

export default AppToolbar;