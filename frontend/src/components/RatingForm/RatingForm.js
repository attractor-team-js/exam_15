import React, {useState} from 'react';
import Grid from "@mui/material/Grid";
import {TextField, Typography} from "@mui/material";
import {Rating} from "@mui/material";
import ButtonWithProgress from "../UI/ButtonWithProgress/ButtonWithProgress";
import {useDispatch, useSelector} from "react-redux";
import {putEstablishments} from "../../store/actions/establishmentsActions";

const RatingForm = ({id}) => {
    const dispatch = useDispatch();
    const loading = useSelector(state => state.establishments.loading);
    const [value, setValue] = useState({
        description: '',
        foodComment: 0,
        serviceComment: 0,
        interiorComment: 0
    });

    const handleChange = (e) => {
        const {name, value} = e.target;
        setValue(prevState => ({
            ...prevState,
            [name]: value,
        }));
    };

    const handleChangeRating = (e, newValue) => {
        const {name} = e.target;
        setValue(prevState => ({
            ...prevState,
            [name]: newValue,
        }))
    }

    const handleClick = (e) => {
        e.stopPropagation();
        e.preventDefault();
        dispatch(putEstablishments({data: value, comment: true, id}));
    }

    return (
        <Grid container flexDirection="column" spacing={1} sx={{paddingY: "15px"}}>
            <Grid item>
                <TextField
                    fullWidth
                    label="Description"
                    value={value.description}
                    name="description"
                    onChange={handleChange}
                    multiline
                    rows={4}
                />
            </Grid>

            <Grid item>
                <Grid container spacing={4} justifyContent="space-between" alignItems="center">
                    <Grid item>
                        <Typography variant="body2">
                            Quality of food:
                        </Typography>

                        <Rating
                            name="foodComment"
                            value={value.foodComment}
                            onChange={handleChangeRating}
                        />
                    </Grid>

                    <Grid item>
                        <Typography variant="body2">
                            Service quality:
                        </Typography>

                        <Rating
                            name="serviceComment"
                            value={value.serviceComment}
                            onChange={handleChangeRating}
                        />
                    </Grid>

                    <Grid item>
                        <Typography variant="body2">
                            Interior:
                        </Typography>

                        <Rating
                            name="interiorComment"
                            value={value.interiorComment}
                            onChange={handleChangeRating}
                        />
                    </Grid>

                    <Grid item>
                        <ButtonWithProgress
                            variant="contained"
                            onClick={handleClick}
                            loading={loading}
                        >
                            Submit review
                        </ButtonWithProgress>
                    </Grid>
                </Grid>
            </Grid>


        </Grid>
    );
};

export default RatingForm;