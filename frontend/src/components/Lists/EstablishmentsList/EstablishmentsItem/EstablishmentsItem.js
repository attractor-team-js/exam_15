import React from 'react';
import {Card, CardActions, CardContent, CardHeader, CardMedia, Rating, Typography} from "@mui/material";
import {apiURL} from "../../../../config";
import dayjs from "dayjs";
import Grid from "@mui/material/Grid";
import PhotoCameraIcon from '@mui/icons-material/PhotoCamera';
import history from "../../../../History";
import {useDispatch, useSelector} from "react-redux";
import Button from "@mui/material/Button";
import {deleteEstablishments} from "../../../../store/actions/establishmentsActions";
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';

const EstablishmentsItem = ({item}) => {
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);

    const result = item.comments.reduce((acc, cur) => {
        return acc + (cur.foodComment + cur.interiorComment + cur.serviceComment)/3;
    }, 0);

    const handleClick = () => {
        history.push(`/establishments/${item._id}`);
    }

    const handleRemove = (e) => {
        e.stopPropagation();
        dispatch(deleteEstablishments(item._id));
    }

    return (
        <Grid item xs={12} md={4} lg={4}>
            <Card sx={{cursor: 'pointer'}} onClick={handleClick}>
                <CardHeader
                    title={item.title}
                    subheader={dayjs(item.date).format('DD-MM-YYYY')}
                />
                <CardMedia
                    component="img"
                    height="194"
                    image={apiURL + '/' + item.mainPic}
                    alt="Kitchen"
                />
                <CardContent>
                    <Grid container flexDirection="column">
                        <Grid item>
                            <Rating name="read-only" value={result} readOnly/>
                        </Grid>

                        <Grid item>
                            <Typography>
                                ( <strong>{result}</strong>, {item.comments.length} reviews )
                            </Typography>
                        </Grid>

                        <Grid item display="flex" justifyItems="center">
                            <PhotoCameraIcon/>

                            <Typography>
                                {item.images.length} photos
                            </Typography>
                        </Grid>
                    </Grid>
                </CardContent>
                {user && user.role === 'admin' ?
                    <CardActions>
                        <Button
                            startIcon={<DeleteForeverIcon/>}
                            onClick={handleRemove}
                        >
                            Remove
                        </Button>
                    </CardActions> : null}
            </Card>
        </Grid>
    );
};

export default EstablishmentsItem;