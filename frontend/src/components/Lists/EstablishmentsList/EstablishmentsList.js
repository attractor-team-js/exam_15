import React from 'react';
import EstablishmentsItem from "./EstablishmentsItem/EstablishmentsItem";

const EstablishmentsList = ({items}) => {
    return items.length !== 0 ? items.map(item => (
        <EstablishmentsItem key={item._id} item={item}/>
    )) : null;
};

export default EstablishmentsList;