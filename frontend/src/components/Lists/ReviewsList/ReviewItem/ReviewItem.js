import React from 'react';
import Grid from "@mui/material/Grid";
import {Divider, Rating, Typography} from "@mui/material";
import dayjs from "dayjs";
import ButtonWithProgress from "../../../UI/ButtonWithProgress/ButtonWithProgress";
import {useDispatch, useSelector} from "react-redux";
import {deleteComment} from "../../../../store/actions/commentsActions";

const ReviewItem = ({review}) => {
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);

    return (
        <Grid item xs={12} sx={{paddingY: "20px"}}>
            <Grid container flexDirection="column">
                <Grid item xs={12}>
                    <Typography variant="body2">
                        On {dayjs(review.date).format('DD/MM/YYYY')}, <strong>{review.user.name}</strong> said:
                    </Typography>

                    <Typography variant="body1">
                        "{review.description}"
                    </Typography>
                </Grid>

                <Grid item xs={12} sm={6} md={4} display="flex" alignItems="center">
                    <Typography variant="body2" component="div" sx={{flexGrow: 1}}>
                        Quality of food:
                    </Typography>

                    <Rating value={review.foodComment} readOnly/>
                    <strong>({review.foodComment})</strong>
                </Grid>

                <Grid item xs={12} sm={6} md={4} display="flex" alignItems="center">
                    <Typography variant="body2" component="div" sx={{flexGrow: 1}}>
                        Service quality:
                    </Typography>

                    <Rating value={review.serviceComment} readOnly/>
                    <strong>({review.serviceComment})</strong>
                </Grid>

                <Grid item xs={12} sm={6} md={4} display="flex" alignItems="center">
                    <Typography variant="body2" component="div" sx={{flexGrow: 1}}>
                        Interior:
                    </Typography>

                    <Rating value={review.interiorComment} readOnly/>
                    <strong>({review.interiorComment})</strong>
                </Grid>

                {user && user._id === review.user._id &&
                    <Grid item xs={12} sm={6} md={4} display="flex" alignItems="center">
                        <ButtonWithProgress
                            variant="contained"
                            color="warning"
                            onClick={() => dispatch(deleteComment({comment: review._id, id: review.establishment}))}
                        >
                            Remove review
                        </ButtonWithProgress>
                    </Grid>}
            </Grid>

            <Divider/>
        </Grid>
    );
};

export default ReviewItem;