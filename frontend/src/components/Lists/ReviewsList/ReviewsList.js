import React from 'react';
import ReviewItem from "./ReviewItem/ReviewItem";
import Grid from "@mui/material/Grid";

const ReviewsList = ({reviews}) => {
    return reviews.length !== 0 ? reviews.map(rew => (
        <Grid container spacing={2} key={rew._id}>
            <ReviewItem review={rew}/>
        </Grid>
    )) : null;
};

export default ReviewsList;