import React, {useState} from 'react';
import Grid from "@mui/material/Grid";
import FileInput from "../UI/FileInput/FileInput";
import ButtonWithProgress from "../UI/ButtonWithProgress/ButtonWithProgress";
import {useDispatch, useSelector} from "react-redux";
import {putEstablishments} from "../../store/actions/establishmentsActions";

const ImageUploadForm = ({id}) => {
    const dispatch = useDispatch();
    const loading = useSelector(state => state.establishments.loading);
    const [value, setValue] = useState({
        image: null,
    });

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setValue(prevState => {
            return {...prevState, [name]: file};
        });
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        const formData = new FormData();
        Object.keys(value).forEach(key => {
            formData.append(key, value[key]);
        });

        dispatch(putEstablishments({data: formData, image: true, id}))
    }

    return (
        <Grid container component="form" onSubmit={handleSubmit} alignItems="center" spacing={4}>
            <Grid item>
                <FileInput
                    name="image"
                    label="Image"
                    value={value.image}
                    onChange={fileChangeHandler}
                />
            </Grid>

            <Grid item>
                <ButtonWithProgress
                    type="submit"
                    loading={loading}
                    variant="contained"
                >
                    Upload
                </ButtonWithProgress>
            </Grid>
        </Grid>
    );
};

export default ImageUploadForm;