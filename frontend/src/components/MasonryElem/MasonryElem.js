import Box from '@mui/material/Box';
import Masonry from '@mui/lab/Masonry';
import {apiURL} from "../../config";
import {IconButton} from "@mui/material";
import HighlightOffIcon from '@mui/icons-material/HighlightOff';
import {useDispatch, useSelector} from "react-redux";
import {deleteImage} from "../../store/actions/imagesActions";

const styles = {
    card: {
        position: "relative",
    },
    btn: {
        position: "absolute",
        top: "-17%",
        right: "-10%"
    }
}

const MasonryElem = ({images, id}) => {
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);

    return (
        <Box sx={{ width: "100%", minHeight: 100 }}>
            <Masonry columns={3} spacing={1}>
                {images.map((item, index) => (
                    <Box key={index} sx={styles.card}>
                        {user && user.role === 'admin' &&
                            <IconButton
                                sx={styles.btn}
                                onClick={() => dispatch(deleteImage({image: item._id, id}))}
                            >
                                <HighlightOffIcon/>
                            </IconButton>}
                        <img
                            src={apiURL + '/' + item.image}
                            alt={item.title}
                            loading="lazy"
                            style={{
                                borderBottomLeftRadius: 4,
                                borderBottomRightRadius: 4,
                                display: 'block',
                                width: '100%',
                            }}
                        />
                    </Box>
                ))}
            </Masonry>
        </Box>
    );
};

export default MasonryElem;