import React from 'react';
import {Route, Routes} from "react-router-dom";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Layout from "./components/UI/Layout/Layout";
import HomePage from "./containers/HomePage/HomePage";
import EstablishmentPage from "./containers/EstablishmentPage/EstablishmentPage";
import AddEstablishmentPage from "./containers/AddEstablishmentPage/AddEstablishmentPage";

const App = () => {
    return (
            <Layout>
                <Routes>
                    <Route path='/' element={<HomePage/>}/>
                    <Route path='/register' element={<Register/>}/>
                    <Route path='/login' element={<Login/>}/>
                    <Route path='/addEstablishment' element={<AddEstablishmentPage/>}/>
                    <Route path='/establishments/:id' element={<EstablishmentPage/>}/>
                </Routes>
            </Layout>
    );
};

export default App;